package com.stone.crm.domain;

import java.math.BigDecimal;
import com.stone.common.annotation.Excel;
import com.stone.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 合同对象 st01_crm_contract
 * 
 * @author stone
 * @date 2024-04-25
 */
public class St01_crm_contract extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private String contractId;

    /** 合同名称 */
    @Excel(name = "合同名称")
    private String contractName;

    /** 合同金额 */
    @Excel(name = "合同金额")
    private BigDecimal money;

    /** 客户 */
    @Excel(name = "客户")
    private String customerId;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contactsId;

    /** 商机 */
    @Excel(name = "商机")
    private String businessId;

    /** 合同类型 */
    @Excel(name = "合同类型")
    private String types;//1：新合同，2：续约合同

    /** 下单日期 */
    @Excel(name = "下单日期")
    private String orderDate;

    /** 开始时间 */
    @Excel(name = "开始时间")
    private String startTime;

    /** 结束时间 */
    @Excel(name = "结束时间")
    private String endTime;

    /** 状态 */
    private String checkStatus;

    /** 审核记录ID */
    private String examineRecordId;

    /** 整单折扣 */
    private BigDecimal discountRate;

    /** 产品总金额 */
    private BigDecimal totalPrice;

    /** 付款方式 */
    private String paymentType;

    /** 只读权限 */
    private String roUserId;

    /** 读写权限 */
    private String rwUserId;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updatedBy;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createdBy;

    /** 负责人 */
    @Excel(name = "负责人")
    private String ownerUserId;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private String createdTime;

    /** 更新时间 */
    private String updatedTime;

    public void setContractId(String contractId) 
    {
        this.contractId = contractId;
    }

    public String getContractId() 
    {
        return contractId;
    }
    public void setContractName(String contractName) 
    {
        this.contractName = contractName;
    }

    public String getContractName() 
    {
        return contractName;
    }
    public void setMoney(BigDecimal money) 
    {
        this.money = money;
    }

    public BigDecimal getMoney() 
    {
        return money;
    }
    public void setCustomerId(String customerId) 
    {
        this.customerId = customerId;
    }

    public String getCustomerId() 
    {
        return customerId;
    }
    public void setContactsId(String contactsId) 
    {
        this.contactsId = contactsId;
    }

    public String getContactsId() 
    {
        return contactsId;
    }
    public void setBusinessId(String businessId) 
    {
        this.businessId = businessId;
    }

    public String getBusinessId() 
    {
        return businessId;
    }
    public void setTypes(String types) 
    {
        this.types = types;
    }

    public String getTypes() 
    {
        return types;
    }
    public void setOrderDate(String orderDate) 
    {
        this.orderDate = orderDate;
    }

    public String getOrderDate() 
    {
        return orderDate;
    }
    public void setStartTime(String startTime) 
    {
        this.startTime = startTime;
    }

    public String getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(String endTime) 
    {
        this.endTime = endTime;
    }

    public String getEndTime() 
    {
        return endTime;
    }
    public void setCheckStatus(String checkStatus) 
    {
        this.checkStatus = checkStatus;
    }

    public String getCheckStatus() 
    {
        return checkStatus;
    }
    public void setExamineRecordId(String examineRecordId) 
    {
        this.examineRecordId = examineRecordId;
    }

    public String getExamineRecordId() 
    {
        return examineRecordId;
    }
    public void setDiscountRate(BigDecimal discountRate) 
    {
        this.discountRate = discountRate;
    }

    public BigDecimal getDiscountRate() 
    {
        return discountRate;
    }
    public void setTotalPrice(BigDecimal totalPrice) 
    {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getTotalPrice() 
    {
        return totalPrice;
    }
    public void setPaymentType(String paymentType) 
    {
        this.paymentType = paymentType;
    }

    public String getPaymentType() 
    {
        return paymentType;
    }
    public void setRoUserId(String roUserId) 
    {
        this.roUserId = roUserId;
    }

    public String getRoUserId() 
    {
        return roUserId;
    }
    public void setRwUserId(String rwUserId) 
    {
        this.rwUserId = rwUserId;
    }

    public String getRwUserId() 
    {
        return rwUserId;
    }
    public void setUpdatedBy(String updatedBy) 
    {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() 
    {
        return updatedBy;
    }
    public void setCreatedBy(String createdBy) 
    {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() 
    {
        return createdBy;
    }
    public void setOwnerUserId(String ownerUserId) 
    {
        this.ownerUserId = ownerUserId;
    }

    public String getOwnerUserId() 
    {
        return ownerUserId;
    }
    public void setCreatedTime(String createdTime) 
    {
        this.createdTime = createdTime;
    }

    public String getCreatedTime() 
    {
        return createdTime;
    }
    public void setUpdatedTime(String updatedTime) 
    {
        this.updatedTime = updatedTime;
    }

    public String getUpdatedTime() 
    {
        return updatedTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("contractId", getContractId())
            .append("contractName", getContractName())
            .append("money", getMoney())
            .append("customerId", getCustomerId())
            .append("contactsId", getContactsId())
            .append("businessId", getBusinessId())
            .append("types", getTypes())
            .append("orderDate", getOrderDate())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("checkStatus", getCheckStatus())
            .append("examineRecordId", getExamineRecordId())
            .append("discountRate", getDiscountRate())
            .append("totalPrice", getTotalPrice())
            .append("paymentType", getPaymentType())
            .append("roUserId", getRoUserId())
            .append("rwUserId", getRwUserId())
            .append("remark", getRemark())
            .append("updatedBy", getUpdatedBy())
            .append("createdBy", getCreatedBy())
            .append("ownerUserId", getOwnerUserId())
            .append("createdTime", getCreatedTime())
            .append("updatedTime", getUpdatedTime())
            .toString();
    }
}
